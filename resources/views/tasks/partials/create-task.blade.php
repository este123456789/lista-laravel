<!-- New Task Form -->
{!! Form::model(new App\Task, ['route' => ['tasks.store'], 'class'=>'form-horizontal', 'role' => 'form']) !!}

    <!-- Task Name -->
    <div class="form-group">
        <label for="task-name" class="col-sm-3 control-label">Nombre de tarea</label>

        <div class="col-sm-6">
            <input type="text" name="name" id="task-name" class="form-control" value="{{ old('task') }}">
        </div>
    </div>

    <!-- Task Description -->
    <div class="form-group">
        <label for="task-description" class="col-sm-3 control-label">Descripcion</label>

        <div class="col-sm-6">
            <textarea name="description" id="task-description" class="form-control" value="{{ old('task') }}" maxlength="155"></textarea>
        </div>
    </div>

    <!-- Task Priority -->

    <div class="form-group">
        <label for="task-priority" class="col-sm-3 control-label">Prioridad</label>


        <div class="col-sm-6">
            <select name="priority" id="task-priority" class="form-control">
                <option value="{{ old('task') }}">{{ old('task') }}</option>
                <option value="Baja">Baja</option>
                <option value="Media">Media</option>
                <option value="Alta">Alta</option>
            </select>
        </div>
    </div>


    <!-- Task End Date -->

    <div class="form-group">
        <label for="task-end_date" class="col-sm-3 control-label">Fecha de vencimiento</label>


        <div class="col-sm-6">
            <input type="date" name="end_date" id="task-end_date" class="form-control" value="{{ old('task') }}">
        </div>
    </div>



    <!-- Task Status -->
    {{--
    <div class="form-group">
        <label for="status" class="col-sm-3 control-label">Estado</label>
        <div class="col-sm-6">
            <div class="checkbox">
                <label for="status">
                    {!! Form::checkbox('completed', 1, null, ['id' => 'status']) !!} Completa
                </label>
            </div>
        </div>
    </div>
    --}}

    <!-- Add Task Button -->
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
             {{Form::button('<span class="fa fa-plus fa-fw" aria-hidden="true"></span> Crear tarea', array('type' => 'submit', 'class' => 'btn btn-default'))}}
        </div>
    </div>

{!! Form::close() !!}

### Laravel 5.2 / lista de tareas / Esteban Villa R.

##### Esteban Villa R. lista de tareas.
|CRUD de usuarios y lista de tareas.|
|por Esteban Villa R.
|Creado en: | 
|Laravel 5.2 [Laravel](http://laravel.com/) 5.2 | 
|Documentaciòn version [5.2] de laravel: (https://laravel.com/docs/5.2/quickstart). |
|[Bootstrap](http://getbootstrap.com) 3.5.x |
|Base de datos [MySQL](https://github.com/mysql)|

###### Prerequisitos
|1.Intalar composer [COMPOSER](https://getcomposer.org/)|
|2.Instalar [git shell](https://git-scm.com/downloads)|
|2.Instalar [PHP](https://www.apachefriends.org/es/index.html)|

###### Instalacion rapida

|(No incluye el entorno dev)|

|1.Ejecute sudo git clone  [lista de tareas](git clone https://este987654321@bitbucket.org/este987654321/listatareas.git) listatareas|

|2.Crear una base de datos MySQL para el proyecto.
mysql -u root -p, si usa Vagrant: mysql -u homestead -psecret
crear laravelTasks base de datos;
\ q|

|3.Desde la raíz de los proyectos ejecute cp .env.example .env
Configure su .env|

|4.Ejecutar sudo composer update desde la carpeta raíz de proyectos
Desde la carpeta raíz de proyectos, ejecute sudo chmod -R 755 ../listatareas|
|5.Desde la carpeta raíz del proyecto ejecuta:|
| composer install|
|6.Desde la carpeta raíz de proyectos ejecuta:|
| php artisan key: generate|
|7.Desde la carpeta raíz de proyectos ejecuta:|
| php artisan migrate |






#### Ver en navegador
1. Desde la carpeta del servidor ejecute `php artisan serve`
2. Abrir en el navegador `http://localhost`


### Rutas del autenticacion y usuario
* ```/```
* ```/auth/login```
* ```/auth/logout```
* ```/auth/register```
* ```/password/reset```

### Rutas del sitio
* ```/home```
* ```/tasks```
* ```/tasks/create```
* ```/tasks/{id}/edit```
* ```/tasks-all```
* ```/tasks-complete```
* ```/tasks-incomplete```

#### Documentacion backend
* https://laravelcollective.com/docs/5.2/html
* http://laravel.com/docs/5.2/authentication
* http://laravel.com/docs/5.2/authorization
* http://laravel.com/docs/5.2/routing
* http://laravel.com/docs/5.0/schema

---
---

## Suerte!!

###### ~ **Esteban Villa**
